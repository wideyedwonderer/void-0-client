import { Injectable } from '@angular/core';
import { TicketService } from 'src/app/shared/ticket.service';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import {  ActivatedRouteSnapshot, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TicketsRouteActivator implements CanActivate {
  path;
  route: ActivatedRouteSnapshot;

  constructor (private readonly ticketService: TicketService, private readonly router: Router) {}

  async canActivate(route: ActivatedRouteSnapshot) {
   return this.ticketService.getSingleTicket(route.params['id']).toPromise()
       .then((value) => {
         if (!value) {
           this.router.navigate(['/404']);
         } else {
           return true;
         }
       });
  }
}
