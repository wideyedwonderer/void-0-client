import { Injectable } from '@angular/core';
import { Ticket } from '../../tickets/models/ticket.model';
import { Resolve} from '@angular/router';
import { TicketService } from 'src/app/shared/ticket.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/shared/user.service';

@Injectable({
  providedIn: 'root'
})
export class UsersTicketsResolver implements Resolve<any> {

  constructor(private readonly ticketService: TicketService, private readonly userService: UserService) {}

  resolve(): Observable<Ticket[]> {
    return this.ticketService.getAllUsersTickets(this.userService.user.email).pipe(map(data => data));
  }
}
