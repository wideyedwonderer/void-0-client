import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { TicketService } from 'src/app/shared/ticket.service';
import { Category } from 'src/app/tickets/models/category.model';


@Injectable({
  providedIn: 'root'
})
export class CategoriesResolver implements Resolve<any> {

  constructor(private readonly ticketService: TicketService) {}

  async resolve(): Promise<Category[]> {
    return this.ticketService.getCategories()
                             .then((categories) => categories)
                             .catch((error) => error);
  }
}
