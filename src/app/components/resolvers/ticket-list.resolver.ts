import { Injectable } from '@angular/core';
import { Ticket } from '../../tickets/models/ticket.model';
import { Resolve, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { TicketService } from 'src/app/shared/ticket.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TicketListResolver implements Resolve<any> {

  constructor(private readonly ticketService: TicketService) {}

  resolve(routeSnapshot: ActivatedRouteSnapshot): Observable<Ticket[]> {
    return this.ticketService.getMultipleTickets(routeSnapshot.queryParams).pipe(map(data => data));
  }
}
