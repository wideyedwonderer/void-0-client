import { Injectable } from '@angular/core';
import { Ticket } from '../../tickets/models/ticket.model';
import { Resolve,  ActivatedRouteSnapshot } from '@angular/router';
import { TicketService } from 'src/app/shared/ticket.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SingleTicketResolver implements Resolve<any> {

  constructor(private readonly ticketService: TicketService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Ticket> {
    return this.ticketService.getSingleTicket(route.params['id']).pipe(map(data => data));
  }
}
