import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { TicketService } from 'src/app/shared/ticket.service';


@Injectable({
  providedIn: 'root'
})
export class LocationsResolver implements Resolve<any> {

  constructor(private readonly ticketService: TicketService) {}

  async resolve(): Promise<string[]> {
    return this.ticketService.getLocations()
                             .then((locations) => locations)
                             .catch((error) => error);
  }
}
