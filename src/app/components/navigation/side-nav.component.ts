import {MediaMatcher} from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy, Input, Output, EventEmitter } from '@angular/core';


/** @title Responsive sidenav */
@Component({
  selector: 'app-side-nav',
  templateUrl: 'side-nav.component.html',
  styleUrls: ['side-nav.component.css'],
})
export class SideNavComponent implements OnDestroy {
  mobileQuery: MediaQueryList;

  @Input()
  navList: [];

  @Output()
  selection: EventEmitter<string>;


  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.selection = new EventEmitter();
  }

  onSelect(value) {
    this.selection.emit(value.name);
  }
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

isString(nav) {
  return typeof nav === 'string';
}
}
