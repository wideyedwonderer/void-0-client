import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
})
export class NavigationComponent implements OnInit {

  user;
constructor(private readonly userService: UserService) {}

ngOnInit(): void {
  this.userService.currentUserSub.subscribe((value) => this.user = value);
}

logout(): void {
this.userService.logout();
}
}
