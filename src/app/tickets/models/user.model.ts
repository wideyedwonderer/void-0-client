import { Ticket } from './ticket.model';

export class User {

  public userId: string;
  public firstName: string;
  public lastName: string;
  public email: string;
  public password: string;
  public banned: boolean;
  public lastLogin: string;
  public roleId: number;
  public receivedTickets?: Ticket[];
  public createdTickets?: User[];
}
