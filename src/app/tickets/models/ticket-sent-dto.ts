export class TicketSendDto {
  public name: string;
  public email: string;
  public ask: boolean;
  public description: string;
  public image: File;
  public location: {name: string};
  public category: {name: string};
}
