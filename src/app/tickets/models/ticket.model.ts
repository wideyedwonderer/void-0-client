import { User } from './user.model';

export class Ticket {
    public ticketId: string;
    public name: string;
    public creator: User;
    public receiver: User;
    public ask: boolean;
    public active: boolean;
    public description: string;
    public imageUrl: string;
    public realized: boolean;
    public dateCreated: Date;
    public dateModified: Date;
    public dateRealized: Date;
    public location: string;
    public category: string;
  }
