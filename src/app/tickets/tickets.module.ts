import { SideNavComponent } from './../components/navigation/side-nav.component';
import { MultipleTicketsComponent } from './multiple-tickets/multiple-tickets';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SingleTicketComponent } from './single-ticket/single-ticket.component';
import {MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { TicketSubmitFormComponent } from './ticket-submit-form/ticket-submit-form.component';
// tslint:disable-next-line:max-line-length
import { MatInputModule, MatButtonModule, MatSelectModule, MatRadioModule, MatCardModule, MatDividerModule, MatGridListModule, MatSidenavModule, MatNavList, MatToolbarModule, MatIconModule, MatListModule } from '@angular/material';
import { TicketsRoutingModule } from './tickets-routing.module';
import { TicketThumbnailComponent } from './multiple-tickets/ticket-thumbnail';
import { UserTicketsComponent } from './user-tickets/user-tickets.component';
import { TicketUpdateComponent } from './single-ticket/ticket-update.component';


@NgModule({
  declarations: [
     SingleTicketComponent,
     TicketSubmitFormComponent,
     MultipleTicketsComponent,
     TicketThumbnailComponent,
     SideNavComponent,
     UserTicketsComponent,
     TicketUpdateComponent
    ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    TicketsRoutingModule,
    MatGridListModule,
    MatDividerModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
  ],
  exports: [],
})
export class TicketsModule { }
