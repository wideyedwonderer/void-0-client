import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TicketSendDto } from '../models/ticket-sent-dto';
import { TicketService } from 'src/app/shared/ticket.service';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-ticket-submit-form',
  templateUrl: './ticket-submit-form.component.html',
  styleUrls: ['./ticket-submit-form.component.css'],
})
export class TicketSubmitFormComponent implements OnInit {

  public ticketsForm = this.fb.group({
    name: [null, [Validators.required, Validators.maxLength(50)]],
    ask: [null, Validators.required],
    description: [null, [Validators.required, Validators.maxLength(300), Validators.minLength(25)]],
    location: [null, Validators.required],
    category: [null, Validators.required],
  });

  ticketToSend: TicketSendDto;

  public categories;

  public locations: string[];
              image: File;
  constructor(
     private fb: FormBuilder,
     private readonly ticketService: TicketService,
     private readonly router: Router,
     private readonly userService: UserService
     ) {}

  async onSubmit(): Promise<void> {
    const ticketDto: TicketSendDto = { ...this.ticketsForm.value};
    const formToSend: FormData = new FormData();
    ticketDto.email = this.userService.user.email;
    Object.keys(ticketDto).forEach((key) => {
      formToSend.append(key, ticketDto[key]);
    } );

    formToSend.append('image', this.image);
    this.ticketService.submitNewTicket(formToSend);
  }


  onImageInput(image: any): void {
    this.image = image;
  }

  async ngOnInit(): Promise<void> {
    this.ticketToSend = new TicketSendDto();
    this.categories = await this.ticketService.getCategories();
    this.locations = await this.ticketService.getLocations();
  }

  parseError(fieldName): string {
    const field = this.ticketsForm.controls[fieldName];
    switch (fieldName) {
      case 'name': {
        if (field.hasError('required')) {
          return `Name is required`;
        }
        if (field.hasError('maxlength')) {
          return `Name's maximum length is ${field.getError('maxlength').requiredLength}`;
        }
        break;
      }
      case 'description': {
        if (field.hasError('required')) {
          return `Description is required`;
        }
        if (field.hasError('maxlength')) {
          return `Description's maximum length is ${field.getError('maxlength').requiredLength}`;
        }
        if (field.hasError('minlength')) {
          return `Description's minimum length is ${field.getError('minlength').requiredLength}`;
        }
        break;
      }
    }
  }
}
