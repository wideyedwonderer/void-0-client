export interface MultipleTicketsQueries {
  ask: number;
  cat: string;
}
