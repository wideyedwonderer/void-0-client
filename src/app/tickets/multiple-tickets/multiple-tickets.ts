import { MultipleTicketsQueries } from './queries.interface';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ticket } from '../models/ticket.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { TicketService } from 'src/app/shared/ticket.service';


@Component({
  selector: 'app-multiple-tickets',
  templateUrl: 'multiple-tickets.html',
  styleUrls: ['multiple-tickets.css']
})
export class MultipleTicketsComponent implements OnInit {
  tickets: Ticket[];
  querySub: Subscription;
  queries: MultipleTicketsQueries = {ask: null, cat: null};
  categories;
  constructor(private readonly ticketService: TicketService,
              private readonly http: HttpClient,
              private readonly route: ActivatedRoute,
              private readonly router: Router) { }

   async ngOnInit() {
      this.tickets = this.route.snapshot.data['tickets'];
      this.queries = {...this.queries, ...this.route.snapshot.queryParams};
      this.categories = await this.ticketService.getCategories();
      this.route.queryParams.subscribe(async (queries) => {
      this.tickets = await this.ticketService.getMultipleTickets(queries).toPromise().then(value => value);
      });
  }
  async onSelect(value) {
    this.queries.cat = value;
    this.router.navigate(['tickets'], {queryParams: {...this.route.snapshot.queryParams, cat: value, page: 1, per_page: 6}});
  }

  paginate(page) {
    this.router.navigate(['tickets'], {queryParams: {...this.route.snapshot.queryParams, ...this.ticketService.paginationQueries[page] } });
  }
}
