import { Component, Input, OnInit, EventEmitter } from '@angular/core';
import { Ticket } from '../models/ticket.model';
import { TicketService } from 'src/app/shared/ticket.service';
import { throttleTime } from 'rxjs/operators';


@Component({
  selector: 'app-ticket-thumbnail',
  templateUrl: 'ticket-thumbnail.html',
  styleUrls: ['ticket-thumbnail.css'],
})
export class TicketThumbnailComponent implements OnInit {

  mouseOver = false;

  mouseOverEmitter = new EventEmitter();
  mouseLeaveEmitter = new EventEmitter();
  @Input() ticket: Ticket;
  constructor(private readonly ticketService: TicketService) {}

  ngOnInit(): void {
    this.mouseOverEmitter.pipe(
      throttleTime(500),
    ).subscribe(() => {
      this.mouseOver = true;
    });

    this.mouseLeaveEmitter.subscribe(() => {
      this.mouseOver = false;
    });
  }
  onMouseOver() {
    this.mouseOverEmitter.emit();
  }
  onMouseLeave() {
    this.mouseLeaveEmitter.emit();
  }

}
