import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Ticket } from '../models/ticket.model';

@Component({
  selector: 'app-user-tickets',
  templateUrl: 'user-tickets.component.html',

})
export class UserTicketsComponent implements OnInit {
tickets: Ticket[];
constructor(private readonly route: ActivatedRoute) {}
ngOnInit(): void {
 this.tickets = this.route.snapshot.data['tickets'];
}

}
