import { UsersTicketsResolver } from './../components/resolvers/user-tickets.resolver';
import { UserTicketsComponent } from './user-tickets/user-tickets.component';
import { MultipleTicketsComponent } from './multiple-tickets/multiple-tickets';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TicketSubmitFormComponent } from './ticket-submit-form/ticket-submit-form.component';
import { SingleTicketComponent } from './single-ticket/single-ticket.component';
import { TicketsRouteActivator } from '../components/guards/tickets-route-activator.service';
import { TicketListResolver } from '../components/resolvers/ticket-list.resolver';
import { SingleTicketResolver } from '../components/resolvers/single-ticket.resolver';
import { CategoriesResolver } from '../components/resolvers/categories.resolver';
import { LocationsResolver } from '../components/resolvers/locations.resolver';
import { TicketUpdateComponent } from './single-ticket/ticket-update.component';



const routes: Routes = [
  { path: '', component: MultipleTicketsComponent, resolve: {tickets: TicketListResolver}},
  { path: 'new', component: TicketSubmitFormComponent, resolve: {categories: CategoriesResolver, locations: LocationsResolver}},
  { path: 'userTickets', component: UserTicketsComponent, resolve: {tickets: UsersTicketsResolver}},
  { path: ':id/update', component: TicketUpdateComponent, resolve: { ticket: SingleTicketResolver }},
  { path: ':id', component: SingleTicketComponent, canActivate: [TicketsRouteActivator], resolve: {ticket: SingleTicketResolver}}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TicketsRoutingModule { }
