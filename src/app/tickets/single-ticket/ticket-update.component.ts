import { TicketSendDto } from './../models/ticket-sent-dto';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { TicketService } from 'src/app/shared/ticket.service';
import { TicketUpdateDTO } from './update-ticket.dto';


@Component({
  selector: 'app-ticket-update',
  templateUrl: './ticket-update.component.html',
  styleUrls: ['ticket-update.component.css']
})
export class TicketUpdateComponent implements OnInit {

  public ticketsForm: FormGroup;
  public categories;
  public locations: string[];
  public ticket: TicketUpdateDTO;

  constructor(
     private fb: FormBuilder,
     private readonly ticketService: TicketService,
     private readonly router: Router,
     private readonly route: ActivatedRoute,
     ) {}

  async onSubmit(): Promise<void> {
    await this.ticketService.updateTicket(this.ticketsForm.value, this.route.snapshot.params['id']);

  }

  async ngOnInit(): Promise<void> {
    this.categories = await this.ticketService.getCategories();
    this.locations = await this.ticketService.getLocations();
    this.ticket = this.route.snapshot.data['ticket'];
    let location = null;
    let category = null;
    if (this.ticket.location) {
      location = this.ticket.location.locationId;
    }
    if (this.ticket.category) {
    category = this.ticket.category.subCategoryId;
    }
    this.ticketsForm = this.fb.group({
      name: [this.ticket.name, [Validators.required, Validators.maxLength(50)]],
      ask: [String(this.ticket.ask), Validators.required],
      description: [this.ticket.description, [Validators.required, Validators.maxLength(300), Validators.minLength(25)]],
      location: [location, Validators.required],
      category: [category, Validators.required],
    });
  }

  parseError(fieldName): string {
    const field = this.ticketsForm.controls[fieldName];
    switch (fieldName) {
      case 'name': {
        if (field.hasError('required')) {
          return `Name is required`;
        }
        if (field.hasError('maxlength')) {
          return `Name's maximum length is ${field.getError('maxlength').requiredLength}`;
        }
        break;
      }
      case 'description': {
        if (field.hasError('required')) {
          return `Description is required`;
        }
        if (field.hasError('maxlength')) {
          return `Description's maximum length is ${field.getError('maxlength').requiredLength}`;
        }
        if (field.hasError('minlength')) {
          return `Description's minimum length is ${field.getError('minlength').requiredLength}`;
        }
        break;
      }
    }
  }
}
