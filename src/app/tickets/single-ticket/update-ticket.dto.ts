export class TicketUpdateDTO {
  public location: {locationId: string, name: string};
  public ask: boolean;
  public description: string;

  public active: boolean;

  public category: {subCategoryId: string, name: string};

  public name: string;
}
