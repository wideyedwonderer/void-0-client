import { TicketUpdateDTO } from './update-ticket.dto';
import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Ticket } from '../models/ticket.model';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { TicketService } from 'src/app/shared/ticket.service';
import { UserService } from 'src/app/shared/user.service';
import { User } from '../models/user.model';

@Component({
  selector: 'app-single-ticket',
  templateUrl: './single-ticket.component.html',
  styleUrls: ['./single-ticket.component.css']
})
export class SingleTicketComponent implements OnInit {

  user: User;
  public ticket: Ticket;

  constructor(
    private readonly ticketService: TicketService,
    private readonly route: ActivatedRoute,
    private readonly userService: UserService) { }

   async ngOnInit() {
    this.ticket = this.route.snapshot.data['ticket'];
    this.user = this.userService.user;
  }

  isUserOwner() {
    if (this.user) {
      return this.user.email === this.ticket.creator.email;
    } else {
      return false;
    }
  }
}
