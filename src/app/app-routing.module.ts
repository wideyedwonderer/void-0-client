import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'tickets',  loadChildren: './tickets/tickets.module#TicketsModule'},
  { path: 'users', loadChildren: './users/users.module#UsersModule'},
  { path: 'error', loadChildren: './errors/errors.module#ErrorsModule'},
  { path: '**', redirectTo: 'home' },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
