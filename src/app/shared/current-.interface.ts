export interface CurrentUser {
  firstName: string;
  lastName: string;
  email: string;
}
