import { IPaginationQueries } from './paginatio-queries.interface';
import { Router } from '@angular/router';
import { Ticket } from './../tickets/models/ticket.model';
import { HttpClient, HttpResponse, HttpHeaderResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Category } from '../tickets/models/category.model';
import { tap, map, catchError } from 'rxjs/operators';
import { UserService } from './user.service';
import { NotificatorService } from './notificator.service';


@Injectable({
  providedIn: 'root'
})
export class TicketService {
  private readonly apiPath = 'http://localhost:3000';
  paginationQueries: IPaginationQueries;
  constructor(
    private readonly http: HttpClient,
    private readonly router: Router,
    private readonly userService: UserService,
    private readonly notificator: NotificatorService) { }

  submitNewTicket(formToSend): Promise<any> {
    return this.http.post(`${this.apiPath}/tickets`, formToSend)
    .toPromise()
    .then((ticket: {id: string}) => {
      this.notificator.success('Successfully added');
      this.router.navigate(['tickets', ticket.id]);
    })
    .catch(error => {
      this.notificator.error('Something went wrong');
      console.log(error);
    });
  }

  updateTicket(ticket: {}, ticketId: string): void {
   this.http.post(`${this.apiPath}/tickets/${ticketId}`, ticket).subscribe({
     next: () => {
       this.router.navigate(['tickets', ticketId]);
       this.notificator.success('Successfully updated');
      },
     error: (error) => {
      this.notificator.error('Something went wrong');
      console.log(error);
     },
   });
  }
  async getCategories(): Promise<Category[]> {
    const cats = await this.http.get(`${this.apiPath}/tickets/categories`).toPromise()
                                .catch((error) => this.router.navigate(['error/444']));
    return cats as Category[];
  }

  async getLocations(): Promise<string[]> {
    const locs = this.http.get(`${this.apiPath}/tickets/locations`).toPromise().catch((error) => this.router.navigate(['error/444']));
    return locs as unknown as string[];
  }

   getSingleTicket(ticketId): Observable<Ticket> {
      return this.http.get<Ticket>(`${this.apiPath}/tickets/${ticketId}`);
  }

  getMultipleTickets(query?: {}): Observable<Ticket[]> {
    let queryString = '?';
    const queryKeysArray = Object.keys(query);
    queryKeysArray.forEach((key) => {
      queryString += key;
      queryString += '=';
      queryString += query[key];
      if ( key !== queryKeysArray[queryKeysArray.length - 1]) {
        queryString += '&';
      }
    });
    const path = this.apiPath + '/tickets' + queryString;
    return this.http.get(`${this.apiPath}/tickets${queryString || ''}`, {observe: 'response'}).
    pipe(
      tap((res: HttpResponse<Ticket[]>) => {
       this.paginationQueries = JSON.parse(res.headers.get('Link'));
      }),
      map(res => <Ticket[]>res.body),
      );
  }

  getAllUsersTickets(userEmail: string): Observable<Ticket[]> {
    return this.http.get<Ticket[]>(`${this.apiPath}/users/${userEmail}/tickets`);
  }
}
