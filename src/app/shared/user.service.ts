import { catchError, concatAll, map } from 'rxjs/operators';
import { NotificatorService } from './notificator.service';
import { Router } from '@angular/router';
import { CurrentUser } from './current-.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { Observable, of } from 'rxjs';
import * as jwt_decode from 'jwt-decode';
import { User } from '../tickets/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

 currentUserSub: EventEmitter<any>;
 user: User;
  constructor(private readonly http: HttpClient, private readonly router: Router, private readonly notificator: NotificatorService) {
    this.currentUserSub = new EventEmitter();
    this.currentUserSub.subscribe((userData) => this.user = userData);
  }

   login(user: {email: string, password: string}): void {
    this.http.post('http://localhost:3000/auth/login', user).subscribe(
      {
        next: (token: string) => {
          localStorage.setItem('token', token);
          this.getUserByEmail(user.email).subscribe({
            next: (userData) => this.currentUserSub.emit(userData),
            error: (error) => {
              this.currentUserSub.emit(null);
            },
            });
            this.notificator.success('Successfully logged in');
            this.router.navigate(['home']);
        },
        error: (error) => {
            this.currentUserSub.emit(null);
            console.log(error);
            this.notificator.error(error.error.message);
        }
      }
    );
  }

  logout(): void {
    this.currentUserSub.emit(null);
    localStorage.clear();
    this.router.navigate(['home']);
  }

      register(user): Observable<any> {
    const options = {headers: new HttpHeaders( {'Content-type': 'application/json'} )};
    return this.http.post('http://localhost:3000/users', user, options)
                    .pipe(
                      catchError((error) => {this.notificator.error('Something went wrong');
                     return of([]);
                    }));

  }

  checkAuth(): void {

    const token = this.getDecodedAccessToken(localStorage.getItem('token'));
    if (token) {
    const tokendate = new Date(0);
    tokendate.setUTCSeconds(token.exp);
    }
    if (token && (new Date(token.exp) <= new Date())) {
      this.getUserByEmail(token.email).subscribe(data => this.currentUserSub.emit(data));
     } else {
      this.currentUserSub.emit(null);
       localStorage.removeItem('token');
       this.logout();
     }

  }

  getUserByEmail(email): Observable<CurrentUser> {
    return this.http.get<CurrentUser>(`http://localhost:3000/users/${email}`);
  }

  private getDecodedAccessToken(token: string): any {
    try {
        return jwt_decode(token);
    }  catch (Error) {
        return null;
    }
  }
}
