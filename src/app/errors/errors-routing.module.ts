import { ErrorPicComponent } from './error-pic.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';


const routes = [
  { path: ':id', component: ErrorPicComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorsRoutingModule {}
