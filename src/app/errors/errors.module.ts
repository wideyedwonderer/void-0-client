import { ErrorsRoutingModule } from './errors-routing.module';
import { ErrorPicComponent } from './error-pic.component';
import { NgModule } from '@angular/core';


@NgModule({
  declarations: [ErrorPicComponent],
  imports: [ErrorsRoutingModule]
})

export class ErrorsModule {}
