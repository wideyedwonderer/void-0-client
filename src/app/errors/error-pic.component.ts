import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error-pic-component',
  template: `<img [src]="imgSrc">`,
  styleUrls: []
})
export class ErrorPicComponent implements OnInit {
  imgSrc;
  constructor(private readonly route: ActivatedRoute) { }

  ngOnInit() {
    this.imgSrc = `https://http.cat/${this.route.snapshot.params['id']}`;
  }

}
