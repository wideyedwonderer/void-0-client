import { UserLoginComponent } from './user-login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserRegisterComponent } from './user-register.component';
import { UserProfileComponent } from './profile/user-profile.component';

const routes: Routes = [
{path: 'register', component: UserRegisterComponent},
{path: 'login', component: UserLoginComponent},
{path: ':email', component: UserProfileComponent},

];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
