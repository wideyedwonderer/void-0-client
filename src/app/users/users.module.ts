import { UserProfileComponent } from './profile/user-profile.component';
import { UserLoginComponent } from './user-login.component';
import { UserRegisterComponent } from './user-register.component';
import { NgModule } from '@angular/core';
import { UserRoutingModule } from './user-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatCardModule, MatButtonModule, MatGridListModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { UserTicketsComponent } from '../tickets/user-tickets/user-tickets.component';



@NgModule({
  declarations: [UserRegisterComponent, UserLoginComponent, UserProfileComponent],

    imports: [ CommonModule, UserRoutingModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule, MatCardModule,
               MatButtonModule, FormsModule, MatGridListModule
  ],
})
export class UsersModule { }
