import { UserService } from 'src/app/shared/user.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
})
export class UserProfileComponent {
  user: any = {
    firstName: null,
    lastName: null,
    email: null,
  };
constructor(private readonly userService: UserService) {
  this.userService.currentUserSub.subscribe((user) => this.user = user);
}
}
