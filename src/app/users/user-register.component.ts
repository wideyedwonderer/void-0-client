import { FormBuilder, Validators, AbstractControl, FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { UserService } from '../shared/user.service';
import { debounceTime } from 'rxjs/operators';

function isMatching(controlName1: string, controlName2: string) {
  return (c: AbstractControl): {[key: string]: boolean} | null => {
    const control1 = c.get(controlName1);
    const control2 = c.get(controlName2);
    if ( control1.value === control2.value) {
      return null;
    }
    return {'match': true};
  };
}

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {

  userRegisterForm = this.fb.group({
    firstName: [null, Validators.required],
    lastName: [null, Validators.required],

    emailGroup: this.fb.group({
            email: [null, [Validators.required, Validators.email]],
            confirmEmail: [null, [Validators.required]]
    }, {
      validator: isMatching('email', 'confirmEmail')
    }),

    passwordGroup: this.fb.group({
            password: [null, [Validators.required,
                              Validators.minLength(7),
                              Validators.pattern(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]/)]],
            confirmPassword: [null, [Validators.required]]
    }, {
      validator: isMatching('password', 'confirmPassword')
    })
  });

  errors = {
    firstName: null,
    lastName: null,
    emailGroup: null,
    email: null,
    confirmEmail: null,
    passwordGroup: null,
    password: null,
    confirmPassword: null,
  };;

  constructor(private readonly fb: FormBuilder, private readonly userService: UserService) { }

 onSubmit() {
    this.userService.register({firstName: this.userRegisterForm.value['firstName'],
    lastName: this.userRegisterForm.value['lastName'],
    email: this.userRegisterForm.value['emailGroup']['email'],
    password: this.userRegisterForm.value['passwordGroup']['password']
    }).subscribe({
      next: (value) => console.log(value + 'HAHA'),
      error: (error) => console.log(error),
});
}

ngOnInit(): void {  
  this.recursivelyAppplyFunctionToAllFormControls(this.userRegisterForm, this.debounceToGiveTimeToUserBeforeErroringOut);
}
  private recursivelyAppplyFunctionToAllFormControls(form: FormGroup, func: (formControl: AbstractControl, controlName?: string) => void) {
    // console.log(form.controls.get())
    Object.keys(form.controls).forEach((key: string) => {
      const abstractControl = form.get(key);
      if(abstractControl instanceof FormGroup) {
       
        this.recursivelyAppplyFunctionToAllFormControls(abstractControl, func);
        func(abstractControl, key);
      } else {
        func(abstractControl, key);
      }
  })
  }

 private debounceToGiveTimeToUserBeforeErroringOut = (formControlToSubscribeTo: AbstractControl, formControlName: string) => {
  formControlToSubscribeTo.valueChanges
                          .pipe(debounceTime(1500))
                          .subscribe(() => {
                              this.errors[formControlName] = null;
                              if (formControlToSubscribeTo.invalid) {
                                this.errors[formControlName] = this.parseError(formControlToSubscribeTo, formControlName);
                              }
                             });
}


private parseError = (field: AbstractControl, fieldName): string => {
  switch (fieldName) {
    case 'firstName': {
      if (field.hasError('required')) {
        return `First Name is required`;
      }
      break;
    }
    case 'lastName': {
      if (field.hasError('required')) {
        return `Last Name is required`;
      }
      break;
    }
    case 'email': {
      if (field.hasError('required')) {
        return `Email is required`;
      }
      if (field.hasError('email')) {
        return `Email is not valid`;
      }
      break;
    }
    case 'emailGroup': {
      if (field.hasError('match')) {
        return `Emails do not match`;
      }
      break;
    }
    case 'password': {
      if (field.hasError('minlength')) {
        return `The password must be at least 8 characters long`;
      }
      if (field.hasError('pattern')) {
        return `The password must have at least one number, one uppercase letter and a special character`;
      }
      break;
    }
    case 'passwordGroup': {
      if (field.hasError('match')) {
        return `Passwords do not match`;
      }
      break;
    }
  }
}

}
